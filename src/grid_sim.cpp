#include <grid_sim/grid_sim.hpp>

#include <iostream>

GridSim::GridSim(): hello("Hello World!") {
	std::cout << hello << std::endl;	
}

void GridSim::advance() {
	std::cout << "advance" << std::endl;
}
