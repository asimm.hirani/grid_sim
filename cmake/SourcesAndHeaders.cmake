set(sources
    src/grid_sim.cpp
)

set(exe_sources
		src/main.cpp
		${sources}
)

set(headers
    include/grid_sim/grid_sim.hpp
)

set(test_sources
  src/grid_sim_test.cpp
)
